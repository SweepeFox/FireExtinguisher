using System;
using UnityEngine;

public class FireExtinguisher : MonoBehaviour
{
    [SerializeField] private ParticleSystem _foamParticles;
    [SerializeField] private Transform _foamSpot;
    [SerializeField] private LayerMask _targetLayer;
    [SerializeField] private float _maxHeight;
    [SerializeField] private float _minHeight;
    [SerializeField] private float _foamCount;
    private FireExtinguisherFoamController _foamController;
    private FireExtinguisherHeightController _heightController;
    private FireExtinguisherModel _model;
    public float MaxHeight => _maxHeight;
    public float MinHeight => _minHeight;
    public FireExtinguisherModel Model => _model;

    public event Action OnInitialized;
    public event Action<float> OnFoamTargetHit;

    private void Start()
    {
        _model = new FireExtinguisherModel(_foamCount);
        _foamController = new FireExtinguisherFoamController(_foamParticles, _foamSpot, _targetLayer);
        _heightController = new FireExtinguisherHeightController(transform, _maxHeight, _minHeight);
        UserInteractEmitter.Instance.OnFoamActivated += _foamController.ActiveFoam;
        UserInteractEmitter.Instance.OnFoamDisactivated += _foamController.DisactiveFoam;
        UserInteractEmitter.Instance.OnHeightChanged += _heightController.OnHeightChanged;
        _model.OnUpdated += OnModelUpdated;
        _foamController.OnFoamTargetHit += OnTargetHit;
        OnInitialized?.Invoke();
    }

    private void Update()
    {
        _model.Update();
    }

    private void FixedUpdate()
    {
        _foamController.FixedUpdate();    
    }

    private void OnDestroy()
    {
        _model.OnDestroy();
        _foamController.OnFoamTargetHit -= OnTargetHit;
        UserInteractEmitter.Instance.OnFoamActivated -= _foamController.ActiveFoam;
        UserInteractEmitter.Instance.OnFoamDisactivated -= _foamController.DisactiveFoam;
        UserInteractEmitter.Instance.OnHeightChanged -= _heightController.OnHeightChanged;
    }

    private void OnModelUpdated()
    {
        if (_model.FoamCount <= 0)
        {
            _foamController.DisactiveFoam();
            _model.OnUpdated -= OnModelUpdated;
            UserInteractEmitter.Instance.OnFoamActivated -= _foamController.ActiveFoam;
            UserInteractEmitter.Instance.OnFoamDisactivated -= _foamController.DisactiveFoam;
        }
    }

    private void OnTargetHit(float foamHeight)
    {
        OnFoamTargetHit?.Invoke(foamHeight);
    }

}
