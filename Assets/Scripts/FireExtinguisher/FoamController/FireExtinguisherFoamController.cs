using System;
using UnityEngine;

public class FireExtinguisherFoamController
{
    public event Action<float> OnFoamTargetHit;
    private readonly ParticleSystem _foamParticles;
    private readonly Transform _foamSpotTransform;
    private readonly LayerMask _targetLayer;
    private bool _isActivated;

    public FireExtinguisherFoamController(ParticleSystem foamParticles, Transform foamSpotTransform, LayerMask targetLayer)
    {
        _foamParticles = foamParticles;
        _foamSpotTransform = foamSpotTransform;
        _targetLayer = targetLayer;
    }

    public void FixedUpdate()
    {
        if (!_isActivated)
        {
            return;
        }
        RaycastHit hit;
        if (Physics.Raycast(_foamSpotTransform.position, _foamSpotTransform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, _targetLayer))
        {
            OnFoamTargetHit?.Invoke(_foamSpotTransform.position.y);
            Debug.DrawRay(_foamSpotTransform.position, _foamSpotTransform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
        }
        else
        {
            Debug.DrawRay(_foamSpotTransform.position, _foamSpotTransform.TransformDirection(Vector3.forward) * 1000, Color.green);
        }
    }

    public void ActiveFoam()
    {
        _foamParticles.Play();
        _isActivated = true;
    }

    public void DisactiveFoam()
    {
        _foamParticles.Stop();
        _isActivated = false;
    }
}
