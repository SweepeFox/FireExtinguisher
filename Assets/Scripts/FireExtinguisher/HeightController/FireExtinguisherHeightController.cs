using UnityEngine;

public class FireExtinguisherHeightController
{
    private readonly Transform _fireExtinguisherTransform;
    private Vector3 _initialPosition;

    public FireExtinguisherHeightController(Transform fireExtinguisherTransform, float maxHeight, float minHeight)
    {
        _fireExtinguisherTransform = fireExtinguisherTransform;
        _initialPosition = _fireExtinguisherTransform.position;
    }

    public void OnHeightChanged(float newHeight)
    {
        _initialPosition.y = newHeight;
        _fireExtinguisherTransform.position = _initialPosition;
    }
}
