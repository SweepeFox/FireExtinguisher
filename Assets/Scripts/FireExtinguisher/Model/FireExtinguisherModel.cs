using System;
using UnityEngine;

public class FireExtinguisherModel
{
    public event Action OnUpdated;
    public float FoamCount => _foamCount;
    private float _foamCount;
    private bool _foamIsActivate;

    public FireExtinguisherModel(float foamCount)
    {
        _foamCount = foamCount;
        UserInteractEmitter.Instance.OnFoamActivated += OnFoamActivated;
        UserInteractEmitter.Instance.OnFoamDisactivated += OnFoamDisactivated;
    }

    public void Update()
    {
        if (!_foamIsActivate || _foamCount == 0)
        {
            return;
        }
        _foamCount = _foamCount < 0 ? 0 : _foamCount - Time.deltaTime;
        OnUpdated?.Invoke();
    }

    private void OnFoamActivated()
    {
        _foamIsActivate = true;
    }

    private void OnFoamDisactivated()
    {
        _foamIsActivate = false;
    }

    public void OnDestroy()
    {
        UserInteractEmitter.Instance.OnFoamActivated -= OnFoamActivated;
        UserInteractEmitter.Instance.OnFoamDisactivated -= OnFoamDisactivated;
    }
}
