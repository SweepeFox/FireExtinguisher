using System.Collections;
using UnityEngine;

public class FoamTarget : MonoBehaviour
{
    [Range(0, 100)]
    [SerializeField] private float _fireHealth;
    [SerializeField] private float _kindlingFireIntervalSeconds;
    [SerializeField] private float _invulnerabilitySeconds;
    [SerializeField] private ParticleSystem _fireParticles;
    private FireExtinguisher _fireExtinguisher;
    private float _defaultDamage;
    private float _increasedDamage;
    private float _loopTime;
    private float _initialFireHealth;
    private bool _canHit = true;


    private void Start()
    {
        _fireExtinguisher = FindObjectOfType<FireExtinguisher>();
        if (_fireExtinguisher == null)
        {
            Debug.LogError($"Fire extinguisher not found in {name}");
            return;
        }
        _defaultDamage = _fireHealth / 100;
        _increasedDamage = _defaultDamage * 2;
        _initialFireHealth = _fireHealth;
        _fireExtinguisher.OnFoamTargetHit += OnHit;
    }

    private void OnHit(float foamHeight)
    {
        if (!_canHit)
        {
            return;
        }
        if (_fireHealth <= 0)
        {
            _fireExtinguisher.OnFoamTargetHit -= OnHit;
            Destroy(_fireParticles.gameObject);
            return;
        }
        var damage = foamHeight > transform.position.y ? _defaultDamage : _increasedDamage;
        _fireHealth -= damage;
        _fireParticles.startSize = _fireHealth / 100;
        StartCoroutine(InvulnerabilityModeRoutine());
    }

    private void Update()
    {
        if (_fireHealth <= 0 || _fireHealth == _initialFireHealth)
        {
            return;
        }
        _loopTime += Time.deltaTime;
        if (_loopTime >= _kindlingFireIntervalSeconds)
        {
            _loopTime = 0;
            _fireHealth++;
            _fireParticles.startSize = _fireHealth / 100;
        }
    }

    private void OnDestroy()
    {
        _fireExtinguisher.OnFoamTargetHit -= OnHit;
    }

    private IEnumerator InvulnerabilityModeRoutine()
    {
        _canHit = false;
        yield return new WaitForSeconds(_invulnerabilitySeconds);
        _canHit = true;
    }
}
