using UnityEngine;
using UnityEngine.EventSystems;

public class FireExtinguisherFoamButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        UserInteractEmitter.Instance.EmitFoamActivated();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        UserInteractEmitter.Instance.EmitFoamDisactivated();
    }
}
