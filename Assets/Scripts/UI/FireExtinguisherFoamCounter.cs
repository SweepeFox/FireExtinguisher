using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FireExtinguisherFoamCounter : MonoBehaviour
{
    private Text _text;
    private FireExtinguisher _fireExtinguisher;

    private void Awake()
    {
        _fireExtinguisher = FindObjectOfType<FireExtinguisher>();
        if (_fireExtinguisher == null)
        {
            Debug.LogError($"Fire extinguisher not found in {name}");
            return;
        }
        _text = GetComponent<Text>();
        _fireExtinguisher.OnInitialized += OnFireExtinguisherInitialized;
    }

    private void OnFireExtinguisherInitialized()
    {
        _fireExtinguisher.Model.OnUpdated += OnFireExtinguisherModelUpdated;
        _fireExtinguisher.OnInitialized -= OnFireExtinguisherInitialized;
        OnFireExtinguisherModelUpdated();
    }

    private void OnFireExtinguisherModelUpdated()
    {
        _text.text = _fireExtinguisher.Model.FoamCount.ToString("0.0");
    }

    private void OnDestroy()
    {
        _fireExtinguisher.Model.OnUpdated -= OnFireExtinguisherModelUpdated;
    }
}
