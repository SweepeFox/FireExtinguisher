using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class FireExtinguisherHeightSlider : MonoBehaviour
{
    private Slider _slider;
    private FireExtinguisher _fireExtinguisher;
    private void Start()
    {
        _slider = GetComponent<Slider>();
        _fireExtinguisher = FindObjectOfType<FireExtinguisher>();
        if (_fireExtinguisher == null)
        {
            Debug.LogError($"Fire extinguisher not found in {name}");
            return;
        }
        _slider.minValue = _fireExtinguisher.MinHeight;
        _slider.maxValue = _fireExtinguisher.MaxHeight;
        _slider.onValueChanged.AddListener(delegate { onValueChanged(); });
    }

    private void onValueChanged()
    {
        UserInteractEmitter.Instance.EmitHeightChanged(_slider.value);
    }

    private void OnDestroy()
    {
        _slider.onValueChanged.RemoveListener(delegate { onValueChanged(); });
    }
}
