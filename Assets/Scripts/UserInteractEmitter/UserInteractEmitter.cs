using System;
using UnityEngine;

public class UserInteractEmitter : MonoBehaviour
{
    public static UserInteractEmitter Instance;
    public event Action OnFoamActivated;
    public event Action OnFoamDisactivated;
    public event Action<float> OnHeightChanged;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    public void EmitFoamActivated()
    {
        OnFoamActivated?.Invoke();
    }

    public void EmitFoamDisactivated()
    {
        OnFoamDisactivated?.Invoke();
    }

    public void EmitHeightChanged(float newHeight)
    {
        OnHeightChanged?.Invoke(newHeight);
    }
}
