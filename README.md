# FireExtinguisher

A demo where you have to put out the flames with a fire extinguisher. If the foam hits the base of the fire, it will extinguish faster. The height of the fire extinguisher is controlled by a slider, the foam is released at the touch of a button. The task is to correctly position the fire extinguisher and put out the fire before the foam runs out.